package com.unicommerce.service.impl;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.unicommerce.service.HeroService;

@Service
public class HeroServiceImpl implements HeroService {

	@Autowired
	private RestTemplate restTemplate;

	@Value("${getHerosByAccId}")
	private String herosByAccountIdUrl;

	@Override
	public Object getListOfHeroByAccountId(String accountId) throws Exception {

		if (accountId.length() <= 0 || accountId.trim().isEmpty()) {
			throw new IllegalArgumentException("Invalid param AcountId - [" + accountId + "]");
		}

		if (herosByAccountIdUrl.length() <= 0 || herosByAccountIdUrl.trim().isEmpty()) {
			throw new IllegalArgumentException("Unable to read getHerosByAccountId URL from application.prop file - ["
					+ herosByAccountIdUrl + "]");
		}

		String replacedUrlWithAccountId = herosByAccountIdUrl.replace("{account_id}", accountId);

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("user-agent", "My Own Rest Client");
		HttpEntity<String> entity = new HttpEntity<>(headers);

		ResponseEntity<Object> response = restTemplate.exchange(replacedUrlWithAccountId, HttpMethod.GET, entity,
				Object.class);

//		Object response = restTemplate.exchange(replacedUrlWithAccountId, HttpMethod.GET, entity, Object.class);

//		JSONArray jsonarray = new JSONArray(response);
//		for (int i = 0; i < jsonarray.length(); i++) {
//		    JSONObject jsonobject = jsonarray.getJSONObject(i);
//		    String name = jsonobject.getString("hero_id");
//		    System.out.println(name);
//		    String url = jsonobject.getString("url");
//		}

//		List<Object> myList = new ArrayList<>();
//		myList.add(response);
//
//		for (int i = 0; i < myList.size(); i++) {
//
//			myList.get(i);
//
//		}

		if (response.getStatusCode() == HttpStatus.OK)
			return response.getBody();

		else
			throw new IllegalArgumentException("Error Occurred while retrieving the data");

	}

}
