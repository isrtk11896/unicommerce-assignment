package com.unicommerce.service;

public interface HeroService {

	Object getListOfHeroByAccountId(String accountId) throws Exception;

}