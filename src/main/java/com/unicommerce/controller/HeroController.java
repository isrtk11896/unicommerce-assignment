package com.unicommerce.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.unicommerce.constant.APIEndPoints;
import com.unicommerce.response.APIResponse;
import com.unicommerce.service.HeroService;

@RestController
@RequestMapping(APIEndPoints.BASEURL)
public class HeroController {

	private static final Logger log = LoggerFactory.getLogger(HeroController.class);

	@Autowired
	private HeroService heroService;

	@GetMapping(value = "/{accountId}",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<APIResponse> getUserChatLogs(HttpServletRequest request,
			@PathVariable(name = "accountId") String accountId) {

		log.info("{} : Recieved request to retieve top heros", request.getSession().getId());

		log.debug("{}: Recieved [accountId] as paramter: {}", request.getSession().getId(), accountId);
		
		try {
			Object  res = heroService.getListOfHeroByAccountId(accountId);
			
			log.info("{} : Retrieved result for accountId [" + accountId + "]: {}",request.getSession().getId());
			
			return new ResponseEntity<>(new APIResponse(res, "Success", true), HttpStatus.OK);
			
		} catch (IllegalArgumentException e) {

			log.error("{} : Got error while retrieving data : {}", request.getSession().getId(),e.getMessage());

			return new ResponseEntity<>(new APIResponse(null, e.getMessage(), false),HttpStatus.BAD_REQUEST);
			
		} catch (Exception e) {
			
			log.error("{} : Unexpected Exception occured : {}", request.getSession().getId(), e.getMessage());
			
			return new ResponseEntity<>(new APIResponse(null, e.getMessage(), false),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
